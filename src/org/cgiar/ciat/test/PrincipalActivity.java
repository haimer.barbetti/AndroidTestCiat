package org.cgiar.ciat.test;

//import com.example.test.R;
import java.util.ArrayList;

import org.cgiar.ciat.test.R;
import org.cgiar.ciat.test.Utility.Validador;
import org.cgiar.ciat.test.model.Recepcion;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;


public class PrincipalActivity extends ActionBarActivity{
	ArrayList<Recepcion> recepcions;
	private  String cultivoSelec="";
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_principal);
	        
	        final SQLConfigManager sqldbHelper=new SQLConfigManager(this);
	        final SQLiteDatabase db=sqldbHelper.getWritableDatabase() ;
	        
	        Button btnAtras=(Button) findViewById(R.id.atras_btn);
	        Button btnGuardar=(Button) findViewById(R.id.guardar_btn);
	        Button btnConsultar=(Button) findViewById(R.id.consultar_btn);
	        Button btnLimpiar=(Button) findViewById(R.id.limpiar_btn);
	        Button btnBorrar=(Button) findViewById(R.id.borrar_btn);
	        Button btnModificar=(Button) findViewById(R.id.modificar_btn);
	        Button btnReportes=(Button) findViewById(R.id.reportes_btn);
	        
	        final EditText editTxtAccesion=(EditText) findViewById(R.id.input_txt_accesion);
	        final EditText editTxtProcedencia=(EditText) findViewById(R.id.input_txt_procedencia);
	        final EditText editTxtContrato=(EditText) findViewById(R.id.input_txt_contrato);
	        final RadioGroup radiosBtnsCultivos=(RadioGroup) findViewById(R.id.input_radioGroup_cultivos);
	        final RadioButton radioFrijol = (RadioButton) radiosBtnsCultivos.getChildAt(0);
	        final RadioButton radioForraje = (RadioButton) radiosBtnsCultivos.getChildAt(1);
	        final RadioButton radioYuca = (RadioButton) radiosBtnsCultivos.getChildAt(2);
	        
			
		editTxtAccesion.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView arg0, int actionId,KeyEvent arg2) {
				String accession_str = arg0.getText().toString();
				if (accession_str.contains("&")) {
					accession_str = Validador.formatAccesion(accession_str);
					editTxtAccesion.setText(accession_str);
				}
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					accession_str = Validador.formatAccesion(accession_str);
					editTxtAccesion.setText(accession_str);
				}
				return false;
			}
		});
	        
	        
	       radiosBtnsCultivos.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
				
					if(checkId==R.id.input_radio_frijol){
						cultivoSelec="frijol";
					}else if(checkId==R.id.input_radio_forraje){
						cultivoSelec="forraje";
					}else if(checkId==R.id.input_radio_yuca){
						cultivoSelec="yuca";
					}
				}
			});
	        
	        btnReportes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					Intent inten = new Intent(view.getContext(),ReportesActivity.class); 
					startActivity(inten);
				}
			});
	        
	        btnAtras.setOnClickListener(new OnClickListener() {
				
				public void onClick(View view) {
					finish();
				    Intent inten = new Intent(view.getContext(),MainActivity.class); 
					startActivity(inten);
				}
			});
	        
	        btnGuardar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					
					if(db!=null){
						Recepcion recepcion=new Recepcion();
						//ContentValues values=new ContentValues();
			        	
						recepcion.setAccesion(editTxtAccesion.getText().toString());
						recepcion.setProcedencia(editTxtProcedencia.getText().toString());
						recepcion.setContrato(editTxtContrato.getText().toString());
						recepcion.setCultivo(cultivoSelec);
						
			        	Toast toast1 =null;
			    		if(!sqldbHelper.existMaterial(recepcion.getAccesion().toString())){
			    			
			    			sqldbHelper.materialAdd(recepcion);
			    			Log.d("AQUI ","INSERTA!");
			    			toast1 =Toast.makeText(getApplicationContext(),
			    	                  "Material Recepcionado!", Toast.LENGTH_SHORT);
			    		}else{
			    			toast1 =Toast.makeText(getApplicationContext(),
			    	                  "No se pudo Recepcionar!", Toast.LENGTH_SHORT);
			    		}
			    		toast1.show();
			        }
				}
			});
	        
	        btnConsultar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					Toast msg = null;
					Recepcion recepcion=null;
					//ArrayList<Recepcion> recepcions=new ArrayList<Recepcion>();
					
					if(editTxtAccesion.getText().toString().equals("")){//Vacio
						
						editTxtAccesion.requestFocus();
						msg=Toast.makeText(getApplicationContext(),
								"El campo Accession esta vacio!", Toast.LENGTH_SHORT);
						msg.show();
						
					}else{//No vacio 
						
						//recepcion=sqldbHelper.materialRead(editTxtAccesion.getText().toString());
						recepcions=(ArrayList<Recepcion>) sqldbHelper.getRecepcionsByAccesion(editTxtAccesion.getText().toString());
						
						if(/*recepcion==null*/recepcions.isEmpty()){//Nullo o No existente
							msg=Toast.makeText(getApplicationContext(),
									"la Accesion no existe!", Toast.LENGTH_SHORT);
							
							msg.show();
							
						}else{
							
							recepcion=(Recepcion)recepcions.get(0);
							
							editTxtAccesion.setText(recepcion.getAccesion());
							editTxtProcedencia.setText(recepcion.getProcedencia());
							editTxtContrato.setText(recepcion.getContrato());
						
							
								if (recepcion.getCultivo().toString().equals("frijol")) {
									radioFrijol.setChecked(true);
								} else if (recepcion.getCultivo().toString().equals("forraje")) {
									radioForraje.setChecked(true);
								} else if (recepcion.getCultivo().toString().equals("yuca")) {
									radioYuca.setChecked(true);
								}
							 
						}
						
						
						
					}
					
					
					
				}
			});
	        
	        
	        btnLimpiar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					editTxtAccesion.setText("");
					editTxtProcedencia.setText("");
					editTxtContrato.setText("");
					
					if(radioFrijol.isChecked()==true){
						radioFrijol.setChecked(false);
					}
					if(radioForraje.isChecked()==true){
						radioForraje.setChecked(false);	
					}
					if(radioYuca.isChecked()==true){
						radioYuca.setChecked(false);
					}
					
					
				}
				
			});
	        
	        btnBorrar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					Toast toast1 =null;
					
					if(editTxtAccesion.getText().toString()==""){
						toast1 =Toast.makeText(getApplicationContext(),
								"Debe llenar el cambpo accesion!", Toast.LENGTH_SHORT);
						
					}else{
						sqldbHelper.materialDelete(editTxtAccesion.getText().toString());
						toast1 =Toast.makeText(getApplicationContext(),
								"Accesion Borrada!", Toast.LENGTH_SHORT);
					}
					toast1.show();
				}
			});
	        
	        
	        btnModificar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					
					Recepcion recepcion=new Recepcion();
					
		        	recepcion.setAccesion(editTxtAccesion.getText().toString());
					recepcion.setProcedencia(editTxtProcedencia.getText().toString());
					recepcion.setContrato(editTxtContrato.getText().toString());
					recepcion.setCultivo(cultivoSelec);
					
					String outToas="";
					Toast toast1 =null;
					if(sqldbHelper.materialUpdate(recepcion)){
						outToas="Accesion Modificada!";
						toast1 =Toast.makeText(getApplicationContext(),
								outToas, Toast.LENGTH_SHORT);
					}else{
						outToas="No se ha podido modificar la Accesion!";
						toast1 =Toast.makeText(getApplicationContext(),
								outToas, Toast.LENGTH_SHORT);
					}
					toast1.show();
					//sqldbHelper.materialUpdate(recepcion);
				}
			});
	 }
	 
	 
	 
	 
	 @Override
	    public void onBackPressed() {
	        return;
	    }
}
