package org.cgiar.ciat.test;

import java.io.Console;

//import com.example.test.R;
import org.cgiar.ciat.test.R;

import android.support.v7.app.ActionBarActivity;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.os.Build;

import org.cgiar.ciat.test.Utility.Validador;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Log.d(" FORMAT ",  Validador.formatAccesion("G    14326"));
        final SQLConfigManager sqldbHelper=new SQLConfigManager(this);
        SQLiteDatabase db=sqldbHelper.getWritableDatabase() ;
        
        if(db!=null){
        	//sqldbHelper.seed(db);
        	/*ContentValues values=new ContentValues();
        	
        	values.put("nombres", "Pastor Lopez");
    		values.put("nombre_usuario", "plopez");
    		values.put("contrasenia", "123456");
    		
    		db.insert("usuarios", null, values);
    		
    		Log.d("AQUI ","INSERTA!");*/
    	}
        
        /*Intent inten = new Intent(getApplicationContext(),ReportesActivity.class);
        startActivity(inten);*/
        
        Button btnAceptar=(Button) findViewById(R.id.aceptar_btn);
        Button btnCancelar=(Button) findViewById(R.id.cancelar_btn);
        final EditText txtUsuario=(EditText) findViewById(R.id.input_txt_user);
        final EditText txtContrasenia=(EditText) findViewById(R.id.input_txt_pass);
        
        
        txtUsuario.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView txt, int actionId, KeyEvent arg2) {
				Toast t=null;
				if(actionId==EditorInfo.IME_ACTION_NEXT){
					
					if(txt.getText().toString().equals("")){
						
						txt.requestFocus();
						t =Toast.makeText(getApplicationContext(),
		    	                  "El Campo de Usuario esta vacio!", Toast.LENGTH_LONG);
						
						t.show();
						
					}
					
				}
				return false;
			}
		});
        
        
        txtContrasenia.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView txt, int actionId, KeyEvent key) {
				Toast t=null;
				if(actionId==EditorInfo.IME_ACTION_NEXT){
					
					if(txt.getText().toString().equals("")){
						
						
						t =Toast.makeText(getApplicationContext(),
		    	                  "El Campo de Contraseña esta vacio!", Toast.LENGTH_LONG);
						
						t.show();
						
					}
					txtContrasenia.requestFocus(actionId);
				}else if(actionId==EditorInfo.IME_ACTION_DONE){
					
				}
				return false;
			}
			}
		);
        
        btnAceptar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View view) {
				Toast t=null;
				Intent inten = new Intent(view.getContext(),PrincipalActivity.class);
				
				Log.d("AQUI Imprime ",txtUsuario.getText().toString()+"\n"
						+ txtContrasenia.getText().toString());
				
				//boolean login=false;
				
				
				if(txtUsuario.getText().length()<=0&&txtContrasenia.getText().length()<=0){
					
					t =Toast.makeText(getApplicationContext(),"Los campos Usuario y Contraseña estan vacios!", Toast.LENGTH_SHORT);
					/*if(txtUsuario.getText().length()<=0){
						t =Toast.makeText(getApplicationContext(),"El campo Usuario esta vacio !", Toast.LENGTH_SHORT);
					    txtUsuario.requestFocus();
					}
					if(txtContrasenia.getText().length()<=0){
						t =Toast.makeText(getApplicationContext(),"El campo Contraseña esta vacio !", Toast.LENGTH_SHORT);
						txtContrasenia.requestFocus();
					}*/
					t.show();
					
				}else if(txtUsuario.getText().length()>0&&txtContrasenia.getText().length()>0){
					
					if(sqldbHelper.login(txtUsuario.getText().toString(),txtContrasenia.getText().toString())){
						inten.putExtra("nombre_usuario", txtUsuario.getText().toString()+"");
						startActivity(inten);
					}else{
						t =Toast.makeText(getApplicationContext(),
				    	                  "No se pudo iniciar Sesion El usuario o contraseña son \n"
				    	                + "Incorrectos o no existen.!", Toast.LENGTH_SHORT);
						t.show();
					}
					
					
				}else{
					
					if(txtUsuario.getText().length()<=0){
						t =Toast.makeText(getApplicationContext(),"El campo Usuario esta vacio !", Toast.LENGTH_SHORT);
					    txtUsuario.requestFocus();
					}
					if(txtContrasenia.getText().length()<=0){
						t =Toast.makeText(getApplicationContext(),"El campo Contraseña esta vacio !", Toast.LENGTH_SHORT);
						txtContrasenia.requestFocus();
					}
					t.show();
					
				}
				
				/*if(sqldbHelper.login(txtUsuario.getText().toString(),txtContrasenia.getText().toString())){
					inten.putExtra("nombre_usuario", txtUsuario.getText().toString()+"");
					startActivity(inten);
				}else{
					t =Toast.makeText(getApplicationContext(),
			    	                  "No se pudo iniciar Sesion El usuario o contraseña son \n"
			    	                + "Incorrectos o no existen.!", Toast.LENGTH_SHORT);
					t.show();
				}*/
				
			}
		});
        
        btnCancelar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				finish();
				//System.exit(0);
			}
			
		});
        
        
        /*if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    
    
    
    /*password.addTextChangedListener(new TextValidator(password) {
        @Override
        public void validate(EditText editText, String text) {
            //Implementamos la validación que queramos
            if( text.length() < 8 )
                password.setError( "La contraseña es muy corta" );
        }*/
    
    
    /**
     * A placeholder fragment containing a simple view.
     */
    
    /*public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }*/

    @Override
    public void onBackPressed() {
        return;
    }
    
}
