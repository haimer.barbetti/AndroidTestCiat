package org.cgiar.ciat.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.cgiar.ciat.test.model.Recepcion;
import org.cgiar.ciat.test.model.Usuario;

import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.content.Entity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;


public class SQLConfigManager extends SQLiteOpenHelper {
	
	private static final String SQL_DROP_USUARIOS_TABLE = "drop table if exists usuarios";
	private static final String SQL_DROP_RECEPCION_TABLE = "drop table if exists usuarios";
	
	private static final String SQL_CREATE_USUARIOS_TABLE = "create table usuarios("
														  + " idUsuario integer primary key autoincrement,"
														  + " nombres text,"
														  + " nombre_usuario text,"
														  + " contrasenia text)";
	
	private static final String SQL_CREATE_RECEPCION_TABLE = "create table recepcion("
														  + " accesion TEXT NOT NULL UNIQUE ,"
														  + " procedencia TEXT,"
														  + " contrato TEXT,"
														  + " cultivo TEXT)";
	
	public SQLConfigManager(Context context) {
		super(context, Environment.getExternalStorageDirectory()+"/test.db", null, 1);
		
	}
	
	public void seed(SQLiteDatabase db){
		
		ContentValues values=new ContentValues();
		values.put("nombres", "Pastor Lopez");
		values.put("nombre_usuario", "plopez");
		values.put("contrasenia", "123456");
		
		db.insert("usuarios", null, values);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_USUARIOS_TABLE);
		db.execSQL(SQL_CREATE_RECEPCION_TABLE);
		Log.d("AQUI ", "onCreate()");
	}

	
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DROP_USUARIOS_TABLE);
		db.execSQL(SQL_CREATE_USUARIOS_TABLE);
	}

	public Usuario getUsuarioById(int id){
		Usuario usuario=new Usuario();
		SQLiteDatabase db = getReadableDatabase();
		Cursor result   = null;
		String[] args = {String.valueOf(id)};
		result  = db.rawQuery("select nombres,nombre_usuario,contrasenia"
				            + " from usuarios where idUsuario=?",args);
		
		usuario.setNombres(result.getString(1));
		usuario.setNombre_usuario(result.getString(2));
		usuario.setContrasenia(result.getString(3));
		return usuario;
	}
	
	public Usuario getUsuarioByNombreUsuario(String nombreUsuario){
		Usuario usuario=new Usuario();
		SQLiteDatabase db = getReadableDatabase();
		Cursor resultCursor   = null;
		String[] args = {nombreUsuario};
		String[] campos={"nombres,nombre_usuario,contrasenia"};
		
		resultCursor=db.query("usuarios", campos, "nombre_usuario=?", args, null, null, null);
		
		//resultCursor.moveToFirst();
		
		if(resultCursor!=null){
			
			resultCursor.moveToFirst();
			
			Log.d(" Cantidad usuarios Recuperados", ""+resultCursor.getCount());
			
			if(resultCursor.getCount()>0){
				
				
				usuario.setNombres(resultCursor.getString(resultCursor.getColumnIndex("nombres")));
				usuario.setNombre_usuario(resultCursor.getString(resultCursor.getColumnIndex("nombre_usuario")));
				usuario.setContrasenia(resultCursor.getString(resultCursor.getColumnIndex("contrasenia")));
				
			}else{
				return usuario;
			}
			
		}
		
		return usuario;
	}
	
	
	
	
	public Boolean existMaterial(String accession){
		SQLiteDatabase db = getReadableDatabase();
		Cursor result   = null;
		String[] args = {accession};
		result  = db.rawQuery("select accesion"
				            + " from recepcion where accesion=?",args);
		
		if(result.moveToFirst()){
			if(result.getString(result.getColumnIndex("accesion"))!=""){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param nom_usuario
	 * @param contrasenia
	 * @return
	 */
	public Boolean login(String nom_usuario,String contrasenia){
		Usuario usuario=new Usuario();
				usuario=getUsuarioByNombreUsuario(nom_usuario.toString());
		//Log.d("AQUI Imprime Usuario",usuario.getNombre_usuario().toString());
		
		  if (  nom_usuario.equals(usuario.getNombre_usuario())
					&& contrasenia.equals(usuario.getContrasenia()) 
					 ) {
				return true;
			} else{
				return false;
			}
			
		}	
	
	
	/**
	 * 
	 * @param usuario
	 * @return
	 */
	public Boolean materialAdd(Recepcion recepcion) {
		SQLiteDatabase db = getReadableDatabase();
		ContentValues values = new ContentValues();
		Cursor resultCursor = null;
		String[] args = { recepcion.getAccesion().toString() };
		String[] campos = { "accesion" };

		resultCursor = db.query("recepcion", campos, "accesion like ?", args,
				null, null, null);
		// resultCursor.moveToFirst();

		values.put("accesion", recepcion.getAccesion().toString());
		values.put("procedencia", recepcion.getProcedencia().toString());
		values.put("contrato", recepcion.getContrato().toString());
		values.put("cultivo", recepcion.getCultivo().toString());

		resultCursor.moveToFirst();
			if (db.insert("recepcion", null, values)!=-1) {
				return true;
			} else {
				return false;
			}

	}
	
	/**
	 * 
	 * @param usuario
	 * @return
	 */
	public Boolean materialUpdate(Recepcion recepcion) {
		SQLiteDatabase db = getReadableDatabase();
		ContentValues values = new ContentValues();
		// Cursor resultCursor = null;
		String[] args = { recepcion.getAccesion().toString() };
		// String[] campos = { "accesion" };
		int actualiza = 0;

		values.put("accesion", recepcion.getAccesion().toString());
		values.put("procedencia", recepcion.getProcedencia().toString());
		values.put("contrato", recepcion.getContrato().toString());
		values.put("cultivo", recepcion.getCultivo().toString());

		actualiza = db.update("recepcion", values, "accesion like ?", args);
		//Log.d("Actualizado ?", String.valueOf(actualiza));

		if (actualiza == 1) {
			Log.d("Actualizado ?", String.valueOf(actualiza));
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param usuario
	 * @return
	 */
	public Boolean materialDelete(String accesion){
		SQLiteDatabase db = getReadableDatabase();
		Cursor result   = null;
		String[] args = {accesion};
		result  = db.rawQuery("delete  from recepcion where accesion=?",args);
		
		if(result.moveToFirst()){
			if(result.getString(result.getColumnIndex("accesion"))!=""){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	
	
	
	
	/**
	 * 
	 * @param accesion
	 * @return Recepcion
	 */
	public Recepcion  materialRead(String accesion){
		
		Recepcion recepcion=new Recepcion();
		SQLiteDatabase db = getReadableDatabase();
		Cursor resultCursor   = null;
		String[] args = {accesion};
		String[] campos={"accesion,procedencia,contrato,cultivo"};
		
		
	    resultCursor=db.query("recepcion", campos, "accesion=?", args, null, null, null);
	    
	    
	    if(resultCursor.moveToFirst()){
	    	
	    	recepcion.setAccesion(resultCursor.getString(resultCursor.getColumnIndex("accesion")));
	    	recepcion.setProcedencia(resultCursor.getString(resultCursor.getColumnIndex("procedencia")));
	    	recepcion.setContrato(resultCursor.getString(resultCursor.getColumnIndex("contrato")));
	    	recepcion.setCultivo(resultCursor.getString(resultCursor.getColumnIndex("cultivo")));
	    	
			Log.d("AQUI RECEPCION", recepcion.toString());
	    }
	    
	    return recepcion;
	}
	
	
	
	/**
	 * consultar todos los registros de recepcion
	 * @return
	 */
	public ArrayList<Recepcion> materialReadAll(){
		
		SQLiteDatabase db = getReadableDatabase();
		Recepcion recepcion=null;
		//List<Recepcion> listMateriales=new ArrayList<Recepcion>();
		ArrayList<Recepcion> listMateriales=new ArrayList<Recepcion>();
		Cursor resultCursor = null;
		String[] campos={"accesion,procedencia,contrato,cultivo"};

		resultCursor  = db.query(false,"recepcion", campos, null, null, null, null, null,null);
          
		resultCursor.moveToFirst();
		if(resultCursor!=null){
		do {
			recepcion=new Recepcion();
			recepcion.setAccesion(resultCursor.getString(resultCursor.getColumnIndex("accesion")));
			recepcion.setProcedencia(resultCursor.getString(resultCursor.getColumnIndex("procedencia")));
			recepcion.setContrato(resultCursor.getString(resultCursor.getColumnIndex("contrato")));
			recepcion.setCultivo(resultCursor.getString(resultCursor.getColumnIndex("cultivo")));
			
			listMateriales.add(recepcion);
			
		} while (resultCursor.moveToNext());
		
		}
		
		/*recepcion.setAccesion(resultCursor.getString(resultCursor.getColumnIndex("accesion")));
		recepcion.setProcedencia(resultCursor.getString(resultCursor.getColumnIndex("procedencia")));
		recepcion.setContrato(resultCursor.getString(resultCursor.getColumnIndex("contrato")));
		recepcion.setCultivo(resultCursor.getString(resultCursor.getColumnIndex("cultivo")));
		
		listMateriales.add(recepcion);
		
		resultCursor.moveToNext();*/
		
		Log.d("Length ",String.valueOf(resultCursor.getCount() ));
		//if (resultCursor.moveToFirst()) {
			//resultCursor.moveToFirst();
			//do {
				
				/*recepcion.setAccesion(resultCursor.getString(resultCursor.getColumnIndex("accesion")));
				recepcion.setProcedencia(resultCursor.getString(resultCursor.getColumnIndex("procedencia")));
				recepcion.setContrato(resultCursor.getString(resultCursor.getColumnIndex("contrato")));
				recepcion.setCultivo(resultCursor.getString(resultCursor.getColumnIndex("cultivo")));
				
				listMateriales.add(recepcion);
				
			} while (resultCursor.moveToNext());*/
		//}
		
		return listMateriales;
		
	}
	
	/**
	 * 
	 * @param accesion
	 * @return
	 */
	public ArrayList<Recepcion> getRecepcionsByAccesion(String accesion){
		
		ArrayList<Recepcion> recepcions=new ArrayList<Recepcion>();
		Recepcion recepcion=new Recepcion();
		SQLiteDatabase db = getReadableDatabase();
		Cursor resultCursor   = null;
		String[] args = {accesion};//,accesion.toUpperCase()
		String[] campos={"accesion,procedencia,contrato,cultivo"};
		
		resultCursor=db.query("recepcion", campos, "accesion like ?", args, null, null, "accesion");
		
		if (resultCursor.moveToFirst()) {
		     
		     do {
		    	 
				recepcion.setAccesion(resultCursor.getString(resultCursor.getColumnIndex("accesion")));
				recepcion.setProcedencia(resultCursor.getString(resultCursor.getColumnIndex("procedencia")));
				recepcion.setContrato(resultCursor.getString(resultCursor.getColumnIndex("contrato")));
				recepcion.setCultivo(resultCursor.getString(resultCursor.getColumnIndex("cultivo")));
		 		
		 		recepcions.add(recepcion);
		     } while(resultCursor.moveToNext());
		}
		
		return recepcions;
	}
	
	
	/**
	 * consultar todos cultivos
	 * @return
	 */
	public List<String> cultivosReadAll(){
		
		SQLiteDatabase db = getReadableDatabase();
		List<String> listCultivos=new ArrayList<String>();
		
		Cursor resultCursor = null;
		String[] campos = { "cultivo" };

		resultCursor  = db.query(true,"recepcion", campos, null, null, null, null, null,null);
		resultCursor.moveToFirst();
		//if (resultCursor.moveToFirst()) {

			do {
				
				listCultivos.add(resultCursor.getString(resultCursor.getColumnIndex("cultivo")).toString());
				
			} while (resultCursor.moveToNext());
		//}
		
		return listCultivos;
		
	}
	
	
	public List<Recepcion> materialReadAllByCultivo(String cultivo){
		SQLiteDatabase db = getReadableDatabase();
		Recepcion recepcion=null;
		List<Recepcion> listMateriales=new ArrayList<Recepcion>();
		Cursor resultCursor = null;
		String[] campos={"accesion,procedencia,contrato,cultivo"};
		String[] args = {cultivo};
		resultCursor  = db.query(false,"recepcion", campos, " cultivo like ?", args, null, null, null,null);
          
		resultCursor.moveToFirst();
		if(resultCursor!=null){
		do {
			recepcion=new Recepcion();
			recepcion.setAccesion(resultCursor.getString(resultCursor.getColumnIndex("accesion")));
			recepcion.setProcedencia(resultCursor.getString(resultCursor.getColumnIndex("procedencia")));
			recepcion.setContrato(resultCursor.getString(resultCursor.getColumnIndex("contrato")));
			recepcion.setCultivo(resultCursor.getString(resultCursor.getColumnIndex("cultivo")));
			
			listMateriales.add(recepcion);
			
		} while (resultCursor.moveToNext());
		
		}
		
		Log.d("Length ",String.valueOf(resultCursor.getCount() ));
		
		return listMateriales;
		
	}
	
}
