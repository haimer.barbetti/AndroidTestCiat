package org.cgiar.ciat.test;

import java.util.ArrayList;
import org.cgiar.ciat.test.R;
//import com.example.test.R;

import org.cgiar.ciat.test.model.Recepcion;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnHoverListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH) public class RecepcionAdapter extends BaseAdapter {

	Context contex;
	private ArrayList<Recepcion> recepcions;
	private static LayoutInflater inflater = null;
	private View viewP;

	public RecepcionAdapter(Context context, ArrayList<Recepcion> recepcions) {
		this.contex = contex;
		this.recepcions = recepcions;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return recepcions.size();
	}

	@Override
	public Object getItem(int itemId) {
		return recepcions.get(itemId);
	}

	@Override
	public long getItemId(int posicion) {
		return posicion;
	}

	@Override
	public View getView(int posicion, View convertView, ViewGroup parent) {
		
		viewP = convertView;
		
		if (convertView == null){
	       // convertView = inflater.inflate(R.layout.row_item, null);
	        
		}
		
		final TextView txtTblAccesion=(TextView) convertView.findViewById(R.id.tbl_recepcion_accesion);
		final TextView txtTblprocedencia=(TextView) convertView.findViewById(R.id.tbl_recepcion_procedencia);
		final TextView txtTblContrato=(TextView) convertView.findViewById(R.id.tbl_recepcion_contrato);
		final TextView txtTblCultivo=(TextView) convertView.findViewById(R.id.tbl_recepcion_cultivo);
		//final Button btnTblBorrar=(Button) convertView.findViewById(R.id.tbl_borrar_btn);
		//final Button btnAux=null;
		//TableRow tblRow =(TableRow) convertView.findViewById(R.id.tbl_row);
		
		
		Recepcion recepcion =new Recepcion();
	    
	    recepcion=recepcions.get(posicion);
	    
	    txtTblAccesion.setText(recepcion.getAccesion());
	    txtTblprocedencia.setText(recepcion.getProcedencia());
	    txtTblContrato.setText(recepcion.getContrato());
	    txtTblCultivo.setText(recepcion.getCultivo());
	    
	    
	    //btnTblBorrar.setVisibility(View.INVISIBLE);
	    
	    
	    
	     /*txtTblAccesion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				
				//btnTblBorrar.setVisibility(View.VISIBLE);
				//Log.d("Click", "Hey ! "+txtTblAccesion.getText().toString());
			}
		});*/
	     
	    
	     
	     /*txtTblprocedencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				//Log.d("Click", "Hey ! "+txtTblprocedencia.getText().toString());
				
			}
		});*/
	     
	     /*btnTblBorrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
			}
		});*/
	    
	     /*tblRow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				//btnTblBorrar.setVisibility(View.GONE); 
				
				TableRow row=(TableRow) view.findViewById(R.id.tbl_row);
				
				for(int i=0;i<row.getChildCount();i++){
					View child = row.getChildAt(i);
				    //child.setEnabled(false);
				    child.setVisibility(View.VISIBLE);
				}
				
				btnTblBorrar.setVisibility(View.VISIBLE);
				Log.d("Click", "Hey ! "+view.findViewById(R.id.tbl_recepcion_accesion));
			}
		});*/
	     
	    return convertView;
		
	}
}
