package org.cgiar.ciat.test;

import java.util.ArrayList;
import org.cgiar.ciat.test.R;
//import com.example.test.R;

import org.cgiar.ciat.test.model.Recepcion;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnHoverListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public abstract class RecepcionListAdapter extends BaseAdapter {
    Context context;
	private ArrayList<?> recepcionsList;
	//private static LayoutInflater inflater = null;
	//private View viewP;
	int viewId;

	public RecepcionListAdapter(Context context,int viewId, ArrayList<?> recepcionsList) {
		this.context = context;
		this.recepcionsList = recepcionsList;
		this.viewId=viewId;
		//inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return recepcionsList.size();
	}

	@Override
	public Object getItem(int itemId) {
		return recepcionsList.get(itemId);
	}

	@Override
	public long getItemId(int posicion) {
		return posicion;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(viewId, null);
		}
		onInput(recepcionsList.get(position), convertView);
		return convertView;

	}

	public abstract void onInput(Object input,View view);
}
