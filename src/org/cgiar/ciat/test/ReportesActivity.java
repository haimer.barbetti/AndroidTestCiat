package org.cgiar.ciat.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.cgiar.ciat.test.R;
//import com.example.test.R;

import org.cgiar.ciat.test.Utility.Validador;
import org.cgiar.ciat.test.model.Recepcion;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ReportesActivity extends ActionBarActivity  {
	
private String selecCultivo="";	
private boolean userIsInteracting;
ArrayList<Recepcion> recepcions;
private int posicion;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reportes);
		
		recepcions =new ArrayList<Recepcion>();
		
		Button btnConsultar =(Button) findViewById(R.id.consultar_btn);
		Button btnAtras=(Button) findViewById(R.id.atras_btn);
		Button btnLimpiar=(Button) findViewById(R.id.limpiar_btn);
		
		final Spinner spnCultivos=(Spinner) findViewById(R.id.spn_cultivos);
		final ListView list=(ListView) findViewById(R.id.lview_resultado_consulta);
		//final GridView gvResultados=(GridView) findViewById(R.id.tbl_resultado_consulta);
		
		//gvResultados.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
		
		final Button btnGenerarReporte=(Button) findViewById(R.id.generar_reporte_btn);
		
		
		
		final EditText editTxtAccesion=(EditText) findViewById(R.id.input_txt_accesion);
		
		
        
        
		final SQLConfigManager sqldbHelper=new SQLConfigManager(this);
        final SQLiteDatabase db=sqldbHelper.getWritableDatabase() ;
        
        
        List<String> cultivos =new ArrayList<String>();
        
		
        cultivos.add("Seleccione Cultivo");
        
        cultivos.addAll(sqldbHelper.cultivosReadAll());
        
		
		
		//ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item,cultivos);
		
		
		
		
		
		ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item,cultivos);
	    arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		spnCultivos.setAdapter(arrayAdapter);
        
		
		
        /*btnLimpiar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				
				list.setAdapter(null);
				recepcions.clear();
			}
		});*/
		
        /*btnAtras.setOnClickListener(new OnClickListener() {
			
			public void onClick(View view) {
				finish();
			    Intent inten = new Intent(view.getContext(),PrincipalActivity.class); 
				startActivity(inten);
			}
		});*/
        
       
        /*btnConsultar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
			   
				recepcions.clear();
				
				if (editTxtAccesion.getText().toString().equals("")) {
					recepcions = sqldbHelper.materialReadAll();
				} else {
					recepcions = sqldbHelper.getRecepcionsByAccesion(editTxtAccesion.getText().toString());
				}
					   
			 
				
			   list.setAdapter(new RecepcionListAdapter(getApplicationContext(),R.layout.reportes_item_row, recepcions) {

					@Override
					public void onInput(final Object input, View view) {

						TextView txtTblAccesion = (TextView) view.findViewById(R.id.tbl_recepcion_accesion);
						txtTblAccesion.setText(((Recepcion) input).getAccesion());
						TextView txtTblprocedencia = (TextView) view.findViewById(R.id.tbl_recepcion_procedencia);
						txtTblprocedencia.setText(((Recepcion) input).getProcedencia());
						TextView txtTblContrato = (TextView) view.findViewById(R.id.tbl_recepcion_contrato);
						txtTblContrato.setText(((Recepcion) input).getContrato());
						TextView txtTblCultivo = (TextView) view.findViewById(R.id.tbl_recepcion_cultivo);
						txtTblCultivo.setText(((Recepcion) input).getCultivo());
						
						LinearLayout l = (LinearLayout)view;
						l.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub					
								CharSequence texto = "Seleccionado: "+((Recepcion) input).getAccesion();
								Toast toast = Toast.makeText(ReportesActivity.this, texto,Toast.LENGTH_SHORT);
								toast.show();
							}
						});
						
					}
				});
			   
			  
			   
			}
		});*/
       
		 /*spnCultivos.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int position, long l) {
				Button btnGenerarReportes = (Button)findViewById(R.id.generar_reporte_btn);
				btnGenerarReportes.setVisibility(View.VISIBLE);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) { }
			 
		 });*/
		
		
		
		
		editTxtAccesion.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(final TextView edit, int actionId,KeyEvent arg2) {
				final Button btnGenerarReportes = (Button)findViewById(R.id.generar_reporte_btn);
				String accession_str = edit.getText().toString();
				
				if (accession_str.contains("&")) {
					accession_str = Validador.formatAccesion(accession_str);
					edit.setText(accession_str);
				}
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					
					accession_str = Validador.formatAccesion(accession_str);
					edit.setText(accession_str);
					
					if(edit.getText().toString()==""||edit.getText().length()<=0){
						
						Toast msg=null;
						btnGenerarReportes.setVisibility(View.INVISIBLE);
	        			list.setAdapter(null);
	        			msg=Toast.makeText(getApplicationContext(),
		    	                  "El Campo de accesion esta vacio!", Toast.LENGTH_SHORT);
	        			msg.show();
	        			edit.requestFocus();
	        			
					}else if(edit.getText().toString()!=""||edit.getText().length()>0){
						
						list.setAdapter(null);
	        			btnGenerarReportes.setVisibility(View.VISIBLE);
	        			
	        			btnGenerarReportes.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View view) {
								LayoutInflater inflater = getLayoutInflater();
						        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.reportes_header_row, list,false);
						        list.addHeaderView(header, null, false);
						        
								recepcions.clear();
								//String item = adapterView.getItemAtPosition(p).toString();
								recepcions = (ArrayList<Recepcion>) sqldbHelper.getRecepcionsByAccesion(edit.getText().toString());
								
								list.setAdapter(new RecepcionListAdapter(getApplicationContext(),R.layout.reportes_item_row, recepcions) {

									@Override
									public void onInput(final Object input, View view) {
										btnGenerarReportes.setVisibility(View.INVISIBLE);
										
										TextView txtTblAccesion = (TextView) view.findViewById(R.id.tbl_recepcion_accesion);
										txtTblAccesion.setText(((Recepcion) input).getAccesion());
										TextView txtTblprocedencia = (TextView) view.findViewById(R.id.tbl_recepcion_procedencia);
										txtTblprocedencia.setText(((Recepcion) input).getProcedencia());
										TextView txtTblContrato = (TextView) view.findViewById(R.id.tbl_recepcion_contrato);
										txtTblContrato.setText(((Recepcion) input).getContrato());
										TextView txtTblCultivo = (TextView) view.findViewById(R.id.tbl_recepcion_cultivo);
										txtTblCultivo.setText(((Recepcion) input).getCultivo());
										
										LinearLayout l = (LinearLayout)view;
										l.setOnClickListener(new OnClickListener() {
											
											@Override
											public void onClick(View arg0) {
												CharSequence texto = "Seleccionado: "+((Recepcion) input).getAccesion();
												Toast toast = Toast.makeText(ReportesActivity.this, texto,Toast.LENGTH_SHORT);
												toast.show();
											}
											
										});

									}
								});
								
							}
						});
					}
				}
				
				
				
				return false;
			}
		});
		
        spnCultivos.setOnItemSelectedListener(new OnItemSelectedListener() {

        	@Override
			public void onItemSelected(AdapterView<?> parent, View view,int posicion, long arg3) {
        	    final AdapterView<?> adapterView=parent;
        	    final int p=posicion;
				//if (userIsInteracting) {
        		final Button btnGenerarReportes = (Button)findViewById(R.id.generar_reporte_btn);
				
        		if(posicion==0){
        			LayoutInflater inflater = getLayoutInflater();
			        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.reportes_header_row, list,false);
        			list.removeHeaderView(header);
        			list.setAdapter(null);
        			btnGenerarReportes.setVisibility(View.INVISIBLE);
        			
        		}else{
        			
        			list.setAdapter(null);
        			btnGenerarReportes.setVisibility(View.VISIBLE);
        			
        			btnGenerarReportes.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View view) {
							
							recepcions.clear();
							String item = adapterView.getItemAtPosition(p).toString();
							recepcions = (ArrayList<Recepcion>) sqldbHelper.materialReadAllByCultivo(item);
							
							list.setAdapter(new RecepcionListAdapter(getApplicationContext(),R.layout.reportes_item_row, recepcions) {

								@Override
								public void onInput(final Object input, View view) {
									btnGenerarReportes.setVisibility(View.INVISIBLE);
									
									TextView txtTblAccesion = (TextView) view.findViewById(R.id.tbl_recepcion_accesion);
									txtTblAccesion.setText(((Recepcion) input).getAccesion());
									TextView txtTblprocedencia = (TextView) view.findViewById(R.id.tbl_recepcion_procedencia);
									txtTblprocedencia.setText(((Recepcion) input).getProcedencia());
									TextView txtTblContrato = (TextView) view.findViewById(R.id.tbl_recepcion_contrato);
									txtTblContrato.setText(((Recepcion) input).getContrato());
									TextView txtTblCultivo = (TextView) view.findViewById(R.id.tbl_recepcion_cultivo);
									txtTblCultivo.setText(((Recepcion) input).getCultivo());
									
									LinearLayout l = (LinearLayout)view;
									l.setOnClickListener(new OnClickListener() {
										
										@Override
										public void onClick(View arg0) {
											CharSequence texto = "Seleccionado: "+((Recepcion) input).getAccesion();
											Toast toast = Toast.makeText(ReportesActivity.this, texto,Toast.LENGTH_SHORT);
											toast.show();
										}
										
									});

								}
							});
							
						}
					});
        			/*recepcions.clear();
					String item = parent.getItemAtPosition(posicion).toString();
					recepcions = (ArrayList<Recepcion>) sqldbHelper.materialReadAllByCultivo(item);
					
					list.setAdapter(new RecepcionListAdapter(getApplicationContext(),R.layout.reportes_item_row, recepcions) {

						@Override
						public void onInput(final Object input, View view) {

							TextView txtTblAccesion = (TextView) view.findViewById(R.id.tbl_recepcion_accesion);
							txtTblAccesion.setText(((Recepcion) input).getAccesion());
							TextView txtTblprocedencia = (TextView) view.findViewById(R.id.tbl_recepcion_procedencia);
							txtTblprocedencia.setText(((Recepcion) input).getProcedencia());
							TextView txtTblContrato = (TextView) view.findViewById(R.id.tbl_recepcion_contrato);
							txtTblContrato.setText(((Recepcion) input).getContrato());
							TextView txtTblCultivo = (TextView) view.findViewById(R.id.tbl_recepcion_cultivo);
							txtTblCultivo.setText(((Recepcion) input).getCultivo());
							
							LinearLayout l = (LinearLayout)view;
							l.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View arg0) {
									CharSequence texto = "Seleccionado: "+((Recepcion) input).getAccesion();
									Toast toast = Toast.makeText(ReportesActivity.this, texto,Toast.LENGTH_SHORT);
									toast.show();
								}
								
							});

						}
					});*/
        		}
        		
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				Button btnGenerarReportes = (Button)findViewById(R.id.generar_reporte_btn);
				btnGenerarReportes.setVisibility(View.GONE);
				list.setAdapter(null);
			}

		});
		
		
		
		
        
	}

	/*@Override
	public void onItemSelected(AdapterView<?> parent, View view, int posicion,long arg3) {
		Toast toast =null;
		
		String item = parent.getItemAtPosition(posicion).toString();
		
		selecCultivo=item.toString();
		
		Log.d("Selecciona ", item);
		
		toast =Toast.makeText(view.getContext(),"Selecciono "+item+ "! ", Toast.LENGTH_SHORT);
		
		toast.show();
	}*/


  
@Override
	public void onUserInteraction() {
	     super.onUserInteraction();
	     userIsInteracting = true;
	}
	
	/*@Override
    public void onBackPressed() {
        return;
    }*/
	
	 /*public void getAllrecepcions(ArrayList<Recepcion> l,SQLConfigManager dbHelper){
     	l=dbHelper.materialReadAll();
     }*/
	
}
