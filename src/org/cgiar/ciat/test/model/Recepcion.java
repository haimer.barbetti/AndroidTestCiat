package org.cgiar.ciat.test.model;

public class Recepcion {
	private String accesion;
	private String procedencia;
	private String contrato;
	private String cultivo;

	public Recepcion() {
	}

	public Recepcion(String accesion, String procedencia, String contrato,
			String cultivo) {
		super();
		this.accesion = accesion;
		this.procedencia = procedencia;
		this.contrato = contrato;
		this.cultivo = cultivo;
	}

	public String getAccesion() {
		return accesion;
	}

	public void setAccesion(String accesion) {
		this.accesion = accesion;
	}

	public String getProcedencia() {
		return procedencia;
	}

	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getCultivo() {
		return cultivo;
	}

	public void setCultivo(String cultivo) {
		this.cultivo = cultivo;
	}

	/*@Override
	public String toString() {
		return "Recepcion [accesion=" + accesion + ", procedencia="
				+ procedencia + ", contrato=" + contrato + ", cultivo="
				+ cultivo + "]";
	}*/
	
	/*@Override
	public String toString() {
		return (accesion + " " + procedencia + " " + contrato + " "+ cultivo + " ").toUpperCase();
	}*/

}
